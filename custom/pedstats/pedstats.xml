<tool id="pedstats" name="PEDSTATS" version="0.1" force_history_refresh="True">
  <description>pedigree validation and summary</description>
  <requirements>
    <requirement type="package" version="0.6.12">pedstats</requirement>
  </requirements>
  <version_command>pedstats | head -n 1</version_command>
  <command interpreter="python">
    pedstats.py
    #for $ui in $input_files
        --data_files=${ui.data_file}
        --pedigree_files=${ui.pedigree_file}
    #end for
    #if $ibd_file
      --ibd_file=$ibd_file
    #end if
    #if str($missing_value_code)
      --missing_value_code="$missing_value_code"
    #end if

    #if $ignore_mendelian_errors
      --ignore_mendelian_errors
    #end if
    #if $chromosome_x
      --chromosome_x
    #end if
    #if $trim
      --trim
    #end if

    #if $hardy_weinberg
      --hardy_weinberg
    #end if
    #if $show_all
      --show_all
    #end if
    #if str($cutoff)
      --cutoff=$cutoff
    #end if

    #if $check_unrelated
      --check_unrelated
    #end if
    #if $check_all
      --check_all
    #end if
    #if $check_founders
      --check_founders
    #end if

    #if str($age)
      --age=$age
    #end if
    #if str($birth)
      --birth=$birth
    #end if

    #if str($min_gap)
      --min_gap=$min_gap
    #end if
    #if str($max_gap)
      --max_gap=$max_gap
    #end if
    #if str($sib_gap)
      --sib_gap=$sib_gap
    #end if

    #if $pairs
      --pairs
    #end if
    #if $rewrite_pedigree
      --rewrite_pedigree
    #end if
    #if $verbose
      --verbose
    #end if
    #if $marker_tables
      --marker_tables
    #end if

    #if $by_family
      --by_family
    #end if
    #if $by_sex
      --by_sex
    #end if

    #if $pdf
      --pdf
    #end if
    #if $marker_pdf
      --marker_pdf
    #end if
    #if $family_pdf
      --family_pdf
    #end if
    #if $trait_pdf
      --trait_pdf
    #end if
    #if $aff_pdf
      --aff_pdf
    #end if

    #if str($min_genos)
      --min_genos=$min_genos
    #end if
    #if str($min_phenos)
      --min_phenos=$min_phenos
    #end if
    #if str($min_covariates)
      --min_covariates=$min_covariates
    #end if
    #if str($affected_for)
      --affected_for=$affected_for
    #end if

    --logfile=$logfile
    --output_id=$logfile.id
    --new_file_path=$__new_file_path__
  </command>

  <inputs>

<!-- INPUT FILES AND BASIC PARAMETERS -->
    <repeat name="input_files" title="Input file" min="1">
       <param name="data_file" type="data" format="txt" label="Input data file (-d)" help="Specify here the input data file, in linkage or QTDT format." />
      <param name="pedigree_file" type="data" format="txt" label="Pedigree file (-p)" help="Specify here the pedigree file, with genotype, phenotype and family structure information." />
    </repeat>

    <param name="ibd_file" type="data" format="txt" optional="true" label="IBD sharing file (-i)" help="See http://www.sph.umich.edu/csg/abecasis/PedStats/reference/IBD.html for further details." />

    <param name="missing_value_code" type="text" value="" label="Missing value code (-x)" help="Selects the missing value code for quantitative phenotypes and covariates in the pedigree file. If possible, it is always safer to replace missing values with “x“, rather than use this option" />


<!-- PEDIGREE FILE -->

    <param name="ignore_mendelian_errors" type="boolean" checked="false" label="Calculate statistics even if Mendelian inconsistencies are present in the pedigree (--ignoreMendelianErrors)" help="The default behavior is to list inconsistencies and stop." />

    <param name="chromosome_x" type="boolean" checked="false" label="Calculate statistics for X-linked loci (--chromosomeX)" help="In this case, males are hemizygous and carry only one chromosome. In the pedigree file they should be encoded as if they carried two identical alleles." />

    <param name="trim" type="boolean" checked="false" label="Remove uninformative individuals from pedigree before calculating statistics (--trim)" help="This option can be combined with the “--rewritePedigree“ option below. Uninformative individuals are individuals with no genotype or phenotype data who are not required to establish relationships between individuals for whom data is available." />


<!-- GENOTYPE CHECKS -->

    <param name="hardy_weinberg" type="boolean" checked="false" label="Hardy-Weinberg equilibrium test (--hardyWeinberg)" help="This option runs two sets of tests for Hardy-Weinberg equilibrium, one among all individuals in the pedigree file and another among a set of unrelated individuals." />

    <param name="show_all" type="boolean" checked="false" label="Print non-significant results for Hardy-Weinberg equilibrium tests (--showAll)" help="The default behavior for Hardy-Weinberg test options is to report only significant results." />

    <param name="cutoff" type="float" value="0.05" optional="true" label="Significance cutoff for Hardy-Weinberg test (--cutoff)" help="This option sets the significance cutoff for graphical and text display of Hardy-Weinberg test results. The default behaviour is to report results for tests with p-value smaller than 0.05." />


<!-- HARDY-WEINBERG SAMPLE -->

    <param name="check_unrelated" type="boolean" checked="false" label="Hardy-Weinberg equilibrium test using genotypes from a set of unrelated individuals (--checkUnrelated)" help="This option runs the Hardy-Weinberg equilibrium test using genotypes from a set of unrelated individuals. Pedstats selects a set of unrelated individuals from your data that will almost always include more independent genotypes per marker than a founders only test. Individuals chosen for the test set are reported in the file (pedstats.hweselection). For details of the selection algorithm see our description of Hardy-Weinberg testing in unrelated samples." />

    <param name="check_all" type="boolean" checked="false" label="Hardy-Weinberg equilibrium test using all genotypes (--checkAll)" help="This option runs the Hardy-Weinberg equilibrium test using all genotypes. As implemented, this test ignores family structure but can be useful when the pool of individuals chosen by the unrelated sample test is small or mostly ungenotyped for one or more markers." />

    <param name="check_founders" type="boolean" checked="false" label="Hardy-Weinberg equilibrium test using only founder genotypes (--checkFounders)" help="" />


<!-- AGE CHECKING -->

    <param name="age" type="text" value="" label="Specifies the label (as written in the .dat file) for an age variable (--age)" help="" />

    <param name="birth" type="text" value="" label="Specifies the label (as written in the .dat file) for a birth year variable (--birth)" help="" />


<!-- GENERATION -->

    <param name="min_gap" type="float" value="13.00" optional="true" label="Specifies the minimum allowable generation gap (--minGap)" help="Differences in parent-offspring ages or birth year that imply a shorter generation gap will be flagged as an error when age checks are run." />

    <param name="max_gap" type="float" value="70.00" optional="true" label="Specifies the maximum allowable generation gap (--maxGap)" help="Differences in parent-offspring ages or birth year that imply a longer generation gap will be flagged as an error when age checks are run." />

    <param name="sib_gap" type="float" value="30.00" optional="true" label="Specifies the maximum allowable gap in age or birth year among siblings (--sibGap)" help="Differences in ages or birth year that imply a longer gap between siblings will be flagged as an error when age checks are run." />


<!-- OUTPUT OPTIONS -->

    <param name="pairs" type="boolean" checked="false" label="Count the number of sib, parent-offspring, avuncular, grandparent-grandchild and cousin pairs in the pedigree (--pairs)" help="For each dichotomous trait, count affected, unaffected, and discordant diagnosed pairs. For covariates and quantitative traits, provide correlations and counts of phenotyped pairs for each relative pair type. When this option is used in conjunction with the --pdf, option, a graphical summary of pairwise data is also provided." />

    <param name="rewrite_pedigree" type="boolean" checked="false" label="Rewrite pedigree after re-organizing families into connected groups of individuals and trimming uninformative individuals (--rewritePedigree)" help="This option works only in conjunction with --trim." />

    <param name="verbose" type="boolean" checked="false" label="Report family and person ids for group members when disconnected family groups are detected within a single family (--verbose)" help="" />

    <param name="marker_tables" type="boolean" checked="false" label="Report summary tables for marker heterozygosity and genotyping rates (--markerTables)" help="When this option is selected, detailed marker information and Hardy-Weinberg test results are redirected to a separate text file (pedstats.markerinfo). For datasets including more than 50 marker genotypes, this option is automatically selected." />


<!-- GROUPING OPTIONS -->

    <param name="by_family" type="boolean" checked="false" label="Break down text output by family (--byFamily)" help="When this option is specified, each type of text summary is written on a per family basis to a separate text file with naming convention pedstats.*.fam, where * identifies the type of summary. For instance, covariate summaries by family be will written to a separate text file called pedstats.cov.fam." />

    <param name="by_sex" type="boolean" checked="false" label="Break down text output by sex (--bySex)" help="When using --pdf or --pairs options, produce sex-specific summaries of affection and trait variables." />


<!-- OPTIONS FOR PDF OUTPUT -->

    <param name="pdf" type="boolean" checked="false" label="Generate charts summarizing contents of pedigree file in Adobe PDF format (--pdf)" help="" />

    <param name="marker_pdf" type="boolean" checked="false" label="Generate PDF output only for markers (--markerPDF)" help="" />

    <param name="family_pdf" type="boolean" checked="false" label="Generate PDF ouput only for family information (--familyPDF)" help="" />

    <param name="trait_pdf" type="boolean" checked="false" label="Generate PDF output only for traits and covariates (--traitPDF)" help="" />

    <param name="aff_pdf" type="boolean" checked="false" label="Generate PDF output only for affection variables (--affPDF)" help="" />


<!-- OPTIONS FOR PEDIGREE FILTERING -->

    <param name="min_genos" type="integer" value="" optional="true" label="Remove individuals from pedigree sample that have fewer than the specified number of genotyped markers (--minGenos)" help="" />

    <param name="min_phenos" type="integer" value="" optional="true" label="Remove individuals from pedigree sample that have fewer than the specified number of phenotyped traits (--minPhenos)" help="" />

    <param name="min_covariates" type="integer" value="" optional="true" label="Remove individuals from pedigree sample that have fewer than the specified number of measured covariates (--minCovariates)" help="" />

    <param name="affected_for" type="text" value="" label="Remove individuals from pedigree sample that are unaffected or have not been diagnosed for the specified affection variable (--affectedFor)" help="" />



  </inputs>

<!-- -->


  <outputs>
    <data name="logfile" format="txt" label="${tool.name} on ${on_string}: output" />
  </outputs>

  <tests>

  </tests>
  <help>
**What it does**

PEDSTATS is a handy tool for quick validation and summary of any pair of pedigree (.ped) and data (.dat) files.

**License and citation**

This Galaxy tool is Copyright © 2013-2014 `CRS4 Srl.`_ and is released under the `MIT license`_.

.. _CRS4 Srl.: http://www.crs4.it/
.. _MIT license: http://opensource.org/licenses/MIT

You can use this tool only if you agree to the license terms of: `PEDSTATS`_.

.. _PEDSTATS: http://www.sph.umich.edu/csg/abecasis/PedStats/

If you use this tool, please cite:

- |Cuccuru2014|_
- |Wigginton2005|_.

.. |Cuccuru2014| replace:: Cuccuru, G., Orsini, M., Pinna, A., Sbardellati, A., Soranzo, N., Travaglione, A., Uva, P., Zanetti, G., Fotia, G. (2014) Orione, a web-based framework for NGS analysis in microbiology. *Bioinformatics* 30(13), 1928-1929
.. _Cuccuru2014: http://bioinformatics.oxfordjournals.org/content/30/13/1928
.. |Wigginton2005| replace:: Wigginton, J. E., and Abecasis, G. R. (2005) PEDSTATS: descriptive statistics, graphics and quality assessment for gene mapping data. *Bioinformatics* 21(16): 3445-3447
.. _Wigginton2005: http://bioinformatics.oxfordjournals.org/content/21/16/3445
  </help>
</tool>
