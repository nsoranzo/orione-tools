<tool id="plink" name="PLINK" version="0.1" force_history_refresh="True">
  <description></description>
  <requirements>
    <requirement type="package" version="1.07">plink</requirement>
  </requirements>
  <version_command>plink --noweb 2>/dev/null | sed -n '3p'</version_command>
  <command interpreter="python">
    plink.py

    --input_type=$input_selection.input_type
    #if str($input_selection.input_type)=="pedmap"
      --ped_file=$input_selection.ped_file
      --map_file=$input_selection.map_file
    #elif str($input_selection.input_type)=="lped"
      --lped_path=$input_selection.file.extra_files_path
      --lped_basefile=$input_selection.file.metadata.base_name
    #elif str($input_selection.input_type)=="bped"
      --pbed_path=$input_selection.bfile.extra_files_path
      --pbed_basefile=$input_selection.bfile.metadata.base_name
    #end if

    #if $extract_file
      --extract_file=$extract_file
    #end if
    #if $read_genome_file
      --read_genome_file=$read_genome_file
    #end if
    #if $within_file
      --within_file=$within_file
    #end if

    #if $assoc
      --assoc
    #end if
    #if $missing
      --missing
    #end if
    #if $cluster
      --cluster
    #end if
    #if $mh
      --mh
    #end if

    #if $allow_no_sex
      --allow_no_sex
    #end if
    #if $hardy
      --hardy
    #end if
    #if $logistic
      --logistic
    #end if

    #if $sex
      --sex
    #end if
    #if $genome
      --genome
    #end if
    #if $make_bed
      --make_bed
    #end if

    #if str($maf)
      --maf=$maf
    #end if
    #if str($mind)
      --mind=$mind
    #end if
    #if str($hwe)
      --hwe=$hwe
    #end if
    #if str($geno)
      --geno=$geno
    #end if
    #if str($ppc)
      --ppc=$ppc
    #end if

    #if str($indep_window_size)
      --indep_window_size=$indep_window_size
    #end if
    #if str($indep_window_step)
      --indep_window_step=$indep_window_step
    #end if
    #if str($indep_threshold)
      --indep_threshold=$indep_threshold
    #end if

    #if str($indep_pairwise_window_size)
      --indep_pairwise_window_size=$indep_pairwise_window_size
    #end if
    #if str($indep_pairwise_window_step)
      --indep_pairwise_window_step=$indep_pairwise_window_step
    #end if
    #if str($indep_pairwise_threshold)
      --indep_pairwise_threshold=$indep_pairwise_threshold
    #end if

    #if str($mds_plot)
      --mds_plot=$mds_plot
    #end if

    --output=$output
    --output_id=$output.id
    --new_file_path=$__new_file_path__
  </command>
  <inputs>
    <conditional name="input_selection">
      <param name="input_type" type="select" label="Select input type" help="">
        <option value="pedmap">Pedigree and genetic map (--ped and --map)</option>
        <option value="lped">Uncompressed data (--file)</option>
        <option value="bped">Compressed data (--bfile)</option>
      </param>
      <when value="pedmap">
        <param name="ped_file" type="data" format="txt" label="Pedigree file (--ped)" help="" />
        <param name="map_file" type="data" format="txt" label="Genetic map (--map)" help="" />
      </when>
      <when value="lped">
        <param name="file" type="data" format="lped" label="Uncompressed data (--file)" help="" />
      </when>
      <when value="bped">
        <param name="bfile" type="data" format="pbed" label="Compressed data (--bfile)" help="" />
      </when>
    </conditional>

    <param name="extract_file" type="data" format="txt" optional="true" label="Extract list of SNPs from file (--extract)" help="" />

    <param name="read_genome_file" type="data" format="txt" optional="true" label="Read previously-computed genome values from file (--read-genome)" help="" />

    <param name="within_file" type="data" format="txt" optional="true" label="Specify clustering scheme from file (--within)" help="" />

    <param name="assoc" type="boolean" checked="false" label="Case/control or QTL association (--assoc)" help="" />

    <param name="missing" type="boolean" checked="false" label="Missing rates (per individual, per SNP) (--missing)" help="" />

    <param name="cluster" type="boolean" checked="false" label="Perform clustering (--cluster)" help="" />

    <param name="mh" type="boolean" checked="false" label="Cochran-Mantel-Haenszel (--mh)" help="" />

    <param name="allow_no_sex" type="boolean" checked="false" label="Do not set ambiguously-sexed individuals missing (--allow-no-sex)" help="" />

    <param name="hardy" type="boolean" checked="false" label="Report Hardy-Weinberg disequilibrium tests (exact) (--hardy)" help="" />

    <param name="logistic" type="boolean" checked="false" label="Test for disease traits and multiple covariates (--logistic)" help="" />

    <param name="sex" type="boolean" checked="false" label="Include sex effect in model (--sex)" help="" />

    <param name="genome" type="boolean" checked="false" label="Calculate IBS distances between all individuals (--genome)" help="" />

    <param name="make_bed" type="boolean" checked="false" label="Make .bed, .fam and .bim (--make-bed)" help="" />

    <param name="maf" type="float" value="" optional="true" label="Minor allele frequency (--maf)" help="" />

    <param name="mind" type="float" value="" optional="true" label="Maximum per-person missing (--mind)" help="" />

    <param name="hwe" type="float" value="" optional="true" label="Hardy-Weinberg disequilibrium p-value (exact) (--hwe)" help="" />

    <param name="geno" type="float" value="" optional="true" label="Maximum per-SNP missing (--geno)" help="" />

    <param name="ppc" type="float" value="" optional="true" label="IBS test p-value threshold (--ppc)" help="" />

    <param name="indep_window_size" type="integer" value="" optional="true" label="Window size for VIF pruning (--indep)" help="" />

    <param name="indep_window_step" type="integer" value="" optional="true" label="Window step for VIF pruning (--indep)" help="" />

    <param name="indep_threshold" type="float" value="" optional="true" label="Threshold for VIF pruning (--indep)" help="" />

    <param name="indep_pairwise_window_size" type="integer" value="" optional="true" label="Window size for r^2 pruning (--indep-pairwise)" help="" />

    <param name="indep_pairwise_window_step" type="integer" value="" optional="true" label="Window step for r^2 pruning (--indep-pairwise)" help="" />

    <param name="indep_pairwise_threshold" type="float" value="" optional="true" label="Threshold for r^2 pruning (--indep-pairwise)" help="" />

    <param name="mds_plot" type="integer" value="" optional="true" label="Multidimensional scaling (--mds-plot)" help="" />
  </inputs>
  <outputs>
    <data name="output" format="txt" label="${tool.name} on ${on_string} " />
  </outputs>
  <tests>
  </tests>
  <help>
**What it does**

PLINK is a whole genome association analysis toolset, designed to perform a range of basic, large-scale analyses in a computationally efficient manner.

**License and citation**

This Galaxy tool is Copyright © 2013-2014 `CRS4 Srl.`_ and is released under the `MIT license`_.

.. _CRS4 Srl.: http://www.crs4.it/
.. _MIT license: http://opensource.org/licenses/MIT

You can use this tool only if you agree to the license terms of: `PLINK`_.

.. _PLINK: http://pngu.mgh.harvard.edu/~purcell/plink/

If you use this tool, please cite:

- |Cuccuru2014|_
- |Purcell2007|_.

.. |Cuccuru2014| replace:: Cuccuru, G., Orsini, M., Pinna, A., Sbardellati, A., Soranzo, N., Travaglione, A., Uva, P., Zanetti, G., Fotia, G. (2014) Orione, a web-based framework for NGS analysis in microbiology. *Bioinformatics* 30(13), 1928-1929
.. _Cuccuru2014: http://bioinformatics.oxfordjournals.org/content/30/13/1928
.. |Purcell2007| replace:: Purcell, S., Neale, B., Todd-Brown, K., Thomas, L., Ferreira, M. A. R., Bender, D., Maller, J., Sklar, P., de Bakker, P. I. W., Daly, M. J., Sham, P. C. (2007) PLINK: A Tool Set for Whole-Genome Association and Population-Based Linkage Analysis. *Am. J. Hum. Genet.*, 81(3), 559-575
.. _Purcell2007: http://www.cell.com/ajhg/abstract/S0002-9297%2807%2961352-4
  </help>
</tool>
