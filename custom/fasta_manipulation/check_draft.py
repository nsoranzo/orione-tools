# -*- coding: utf-8 -*-
"""
Check bacterial draft
"""

import math
import optparse
import os
import re

def fasta_size(filename):
    """ Determine the length of a FASTA file containing only 1 sequence"""
    tmp = 0
    with open(filename) as fasta:
        fasta.next() # skip header
        try:
            while True:
                row = fasta.next().strip()
                tmp += len(row)
        except StopIteration:
            return tmp


def average(values):
    """ Arithmetic mean of a list of values """
    return math.fsum(values) / len(values) if len(values) else float('nan')


def median(values):
    """ Median of a list of values """
    if len(values) == 0:
        return float('nan')
    sorted_values = sorted(values)
    if len(sorted_values) % 2 == 1:
        return sorted_values[(len(sorted_values)-1) / 2]
    else:
        lower = sorted_values[len(sorted_values)/2 - 1]
        upper = sorted_values[len(sorted_values)/2]
        return float(lower + upper) / 2


def N50(values):
    """ N50 of a list of contig lengths, i.e. the length for which the set of all contigs of that length or longer contains at least half of the total of the lengths of the contigs, and for which the set of all contigs of that length or shorter contains at least half of the total of the lengths of the contigs. When 2 values meets both these criteria then the N50 is their average. """
    if len(values) == 0:
        return float('nan')
    sorted_values = sorted(values)
    assembled = math.fsum(values)
    index = 0
    tmp50 = sorted_values[index]
    while tmp50 < assembled / 2:
        index += 1
        tmp50 += sorted_values[index]
    return sorted_values[index] if tmp50 > assembled / 2 else float(sorted_values[index] + sorted_values[index + 1]) / 2


def NG50(values, genomesize):
    """ NG50 of a list of contig lengths, i.e. the length for which the set of all contigs of that length or longer contains at least half of the given genome size. """
    if len(values) == 0:
        return float('nan')
    rev_sorted_values = sorted(values, reverse=True)
    index = 0
    tmpG50 = rev_sorted_values[index]
    try:
        while tmpG50 < float(genomesize) / 2:
            index += 1
            tmpG50 += rev_sorted_values[index]
        return rev_sorted_values[index]
    except IndexError:
        return 0


def __main__():
    parser = optparse.OptionParser()
    parser.add_option('-d', dest='draft', help='draft genome')
    parser.add_option('-r', dest='reference', help='reference genome')
    parser.add_option('-o', dest='summary', help='report file ')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')

    draft = options.draft
    reference = options.reference
    summary = options.summary

    genomesize = fasta_size(reference)
    fasta = ""
    with open(draft, 'r') as infile:
        infile.next() # skip header
        try:
            while True:
                row = infile.next().strip().lower()
                fasta += row
        except StopIteration:
            fasta.replace("\n", "")

    ns = fasta.count("n")
    nsperc =  float(ns) / len(fasta) * 100
    gaps = re.compile('[nx]+')
    gapsiterator = gaps.findall(fasta)
    gap_lengths = [len(gap) for gap in gapsiterator]
    contig = re.compile('[acgt]+')
    contigiterator = contig.findall(fasta)
    contig_lengths = [len(contig) for contig in contigiterator]
    with open(summary, 'w') as out:
        out.write("# Draft Evaluator v1.0 on file %s\n" % os.path.basename(draft) )
        out.write("Reference file:\t%s\n" % reference)
        out.write("Reference length:\t%d\n" % genomesize)
        out.write("Consensus full length:\t%d\n" % len(fasta) )
        out.write("Consensus assembled length:\t%d\n" % sum(contig_lengths) )
        out.write("%% sequenced:\t%s\n" % (100 - round(nsperc, 1)) )
        out.write("%% gaps:\t%s\n" % round(nsperc, 1) )
        out.write("N. contigs:\t%d\n" % len(contigiterator) )
        if len(contigiterator):
            out.write("Average contig length:\t%d\n" % round(average(contig_lengths)) )
            out.write("Median contig length:\t%s\n" % median(contig_lengths) )
            out.write("Maximum contig length:\t%d\n" % max(contig_lengths) )
            n_contigs_over200 = len([_ for _ in contig_lengths if _ >= 200])
            out.write("N. contigs >= 200 bp:\t%d (%s %%)\n" % (n_contigs_over200, round((float(n_contigs_over200) / len(contigiterator) * 100), 1) ) )
            n_contigs_over2000 = len([_ for _ in contig_lengths if _ >= 2000])
            out.write("N. contigs >= 2,000 bp:\t%d (%s %%)\n" % (n_contigs_over2000, round((float(n_contigs_over2000) / len(contigiterator) * 100), 1) ) )
            out.write("N50:\t%s\n" % N50(contig_lengths) )
            out.write("NG50:\t%d\n" % NG50(contig_lengths, genomesize) )
        out.write("N. gaps:\t%s\n" % str(len(gapsiterator)) )
        if len(gapsiterator):
            out.write("Average gap length:\t%d\n" % round(average(gap_lengths)) )
            out.write("Median gap length:\t%s\n" % median(gap_lengths) )
            out.write("Maximum gap length:\t%d\n" % max(gap_lengths) )
            gaps_below5 = len([_ for _ in gap_lengths if _ <= 5])
            out.write("N. gaps <= 5 bp:\t%d (%s %%)\n" % (gaps_below5, str(round((float(gaps_below5) / len(gapsiterator) * 100), 1)) ) )


if __name__ == "__main__":
    __main__()
