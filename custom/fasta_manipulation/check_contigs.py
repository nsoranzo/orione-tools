# -*- coding: utf-8 -*-
"""
Check bacterial contigs
"""

import math
import optparse
import os

def average(values):
    """ Arithmetic mean of a list of values """
    return math.fsum(values) / len(values) if len(values) else float('nan')


def median(values):
    """ Median of a list of values """
    if len(values) == 0:
        return float('nan')
    sorted_values = sorted(values)
    if len(sorted_values) % 2 == 1:
        return sorted_values[(len(sorted_values)-1) / 2]
    else:
        lower = sorted_values[len(sorted_values)/2 - 1]
        upper = sorted_values[len(sorted_values)/2]
        return float(lower + upper) / 2


def N50(values):
    """ N50 of a list of contig lengths, i.e. the length for which the set of all contigs of that length or longer contains at least half of the total of the lengths of the contigs, and for which the set of all contigs of that length or shorter contains at least half of the total of the lengths of the contigs. When 2 values meets both these criteria then the N50 is their average. """
    if len(values) == 0:
        return float('nan')
    sorted_values = sorted(values)
    assembled = math.fsum(values)
    index = 0
    tmp50 = sorted_values[index]
    while tmp50 < assembled / 2:
        index += 1
        tmp50 += sorted_values[index]
    return sorted_values[index] if tmp50 > assembled / 2 else float(sorted_values[index] + sorted_values[index + 1]) / 2


def NG50(values, genomesize):
    """ NG50 of a list of contig lengths, i.e. the length for which the set of all contigs of that length or longer contains at least half of the given genome size. """
    if len(values) == 0:
        return float('nan')
    rev_sorted_values = sorted(values, reverse=True)
    index = 0
    tmpG50 = rev_sorted_values[index]
    try:
        while tmpG50 < float(genomesize) / 2:
            index += 1
            tmpG50 += rev_sorted_values[index]
        return rev_sorted_values[index]
    except IndexError:
        return 0


class FastaStream(object):
    """
    Generate a stream of 'FASTA strings' from an I/O stream.
    """

    def __init__(self, multifastafile):
        self.infile = open(multifastafile)
        self.buffer = []

    def __del__(self) :
        self.infile.close()

    def __iter__(self):
        return self

    def next(self):
        """ Return next FASTA string """
        while True:
            try:
                line = self.infile.next()
                if line.startswith('>') and self.buffer:
                    fasta = "".join(self.buffer)
                    self.buffer = [line]
                    return fasta
                else:
                    self.buffer.append(line)
            except StopIteration:
                if self.buffer:
                    fasta = "".join(self.buffer)
                    self.buffer = []
                    return fasta
                else:
                    raise StopIteration


def __main__():
    parser = optparse.OptionParser()
    parser.add_option('-f', dest='fasta', help='Contigs FASTA file')
    parser.add_option('-g', dest='genomesize', type='float', help='Expected genome size')
    parser.add_option('-o', dest='summary', help='Report file')
    (options, args) = parser.parse_args()
    if len(args) > 0:
        parser.error('Wrong number of arguments')

    infilename = options.fasta
    genomesize_bp = round(options.genomesize * 1000000)
    summary = options.summary

    contig_lengths = []
    fastaStream = FastaStream(infilename)
    for fasta in fastaStream:
        seq = "".join(fasta.split("\n")[1:])
        contig_lengths.append(len(seq))
    with open(summary, 'w') as out:
        out.write("# Contigs Evaluator v1.0 on file %s\n" % os.path.basename(infilename) )
        out.write("Estimated genome size:\t%d bp\n" % genomesize_bp)
        out.write("Assembled nucleotides:\t%d bp\n" % sum(contig_lengths) )
        out.write("Estimated coverage:\t%s x\n" % round(float(sum(contig_lengths)) / genomesize_bp, 2) )
        out.write("N. contigs:\t%d\n" % len(contig_lengths) )
        if len(contig_lengths):
            out.write("Average contig length:\t%d\n" % round(average(contig_lengths)) )
            out.write("Median contig length:\t%d\n" % round(median(contig_lengths)) )
            out.write("Maximum contig length:\t%d\n" % max(contig_lengths) )
            n_contigs_over200 = len([_ for _ in contig_lengths if _ >= 200])
            out.write("N. contigs >= 200 bp:\t%d (%s %%)\n" % (n_contigs_over200, round((float(n_contigs_over200) / len(contig_lengths) * 100), 1) ) )
            n_contigs_over2000 = len([_ for _ in contig_lengths if _ >= 2000])
            out.write("N. contigs >= 2,000 bp:\t%d (%s %%)\n" % (n_contigs_over2000, round((float(n_contigs_over2000) / len(contig_lengths) * 100), 1) ) )
            out.write("N50:\t%s\n" % N50(contig_lengths) )
            out.write("NG50:\t%s\n" % NG50(contig_lengths, genomesize_bp) )


if __name__ == "__main__":
    __main__()
